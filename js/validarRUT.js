 <script src="js/jquery.rut.js"></script>
    <script type="text/javascript">
      $(function() {
        $("#rut").rut({formatOn: 'keyup', 
        validateOn: 'keyup'
        }).on('rutInvalido', 
        function(){
            rut.setCustomValidity("RUT Inválido");
            $(".form-rut").addClass("has-danger")
            $(".input-rut").addClass("form-control-danger")  
        }).on('rutValido', 
        function(){ 
            $(".form-rut").removeClass("has-danger")
            $(".form-rut").addClass("has-success")
            $(".input-rut").removeClass("form-control-danger")
            $(".input-rut").addClass("form-control-success")
            rut.setCustomValidity('')
        });
      });
            $('#nom').on('input', function() {
                var value = $(this).val();
                if(value.length >= 5){
                    $(".form-nom").removeClass("has-danger")
                    $(".form-nom").addClass("has-success")
                    $(".input-nom").removeClass("form-control-danger")
                    $(".input-nom").addClass("form-control-success")
                    nom.setCustomValidity('')
                }
                if(value.length >= 0 && value.length < 5){
                    nom.setCustomValidity("El Nombre debe tener mas de 5 caracteres");
                    $(".form-nom").addClass("has-danger")
                    $(".input-nom").addClass("form-control-danger")    
                }
        });
    </script>